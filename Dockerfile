FROM jejem/php:7.3-apache

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install \
	cron

RUN apt-get clean && \
	apt-get autoclean && \
	rm -rf /var/lib/apt/lists/*

RUN echo "post_max_size = 550M" >> /etc/php/7.3/mods-available/custom.ini && \
	echo "upload_max_filesize = 550M" >> /etc/php/7.3/mods-available/custom.ini

COPY --chown=www-data:www-data . /var/www/html
RUN rm -f /var/www/html/index.html
RUN ln -sf /etc/sheepit-mirror/config.local.inc.php /var/www/html/includes/config.local.inc.php

RUN runuser -s /bin/bash -c "cd /var/www/html/lib && composer install" - www-data

COPY docker/etc/cron.d/sheepit-mirror /etc/cron.d/
RUN chmod 644 /etc/cron.d/sheepit-mirror
COPY docker/entrypoint.sh /
RUN chmod 755 /entrypoint.sh

VOLUME ["/etc/sheepit-mirror", "/tmp/projects", "/tmp/logs"]
