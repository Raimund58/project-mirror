<?php
/**
* Copyright (C) 2017 Laurent CLOUET
* Author Laurent CLOUET <laurent.clouet@nopnop.net>
**/

require_once(dirname(__FILE__).'/includes/core.inc.php');

function result_download_request($path, $filename, $send_attachment, $expiration = 0) {
	if (file_exists($path) == false) {
		header('HTTP/1.1 404 Not Found');
		die();
	}
	
	header('Expires: '.$expiration);
	header('Cache-Control: must-revalidate');
	header('Pragma: public');
	header('Content-Transfer-Encoding: binary');
	header('Content-Description: File Transfer');
	header('Accept-Ranges: bytes');
	if ($filename != '' && $send_attachment) {
		header('Content-Disposition: attachment; filename="'.$filename.'"');
	}
	$finfo = finfo_open(FILEINFO_MIME_TYPE);
	$type = finfo_file($finfo, $path);
	finfo_close($finfo);
	if (is_string($type)) {
		header('Content-Type: '.$type);
	}
	
	$range = '';
	if (array_key_exists('HTTP_RANGE', $_SERVER)) {
		list($size_unit, $range_orig) = explode('=', $_SERVER['HTTP_RANGE'], 2);
		if ($size_unit == 'bytes') {
			$range = '';
			$ranges = explode(',', $range_orig);
			$range = $ranges[0];
		}
		else {
			header('HTTP/1.1 416 Requested Range Not Satisfiable');
			die();
		}
	}
	
	$file_size = filesize($path);
	$seek_start = 0;
	$seek_end = $file_size - 1;
	$ranges = explode('-', $range);
	$seek_start = $ranges[0];
	if (count($ranges) == 2 && $ranges[1] != '') {
		$seek_end = $ranges[1];
	}
	
	$seek_start = max(abs(intval($seek_start)), 0);
	$seek_end = min(abs(intval($seek_end)), ($file_size-1));
	
	if ($seek_start > 0 || $seek_end < ($file_size-1)) {
		header('HTTP/1.1 206 Partial Content');
		header('Content-Range: bytes '.$seek_start.'-'.$seek_end.'/'.$file_size);
		header('Content-Length: '.(($seek_end-$seek_start)+1));
	}
	else {
		header('Content-Length: '.$file_size);
	}
	
	ob_clean();
	flush();
	
	set_time_limit(0);
	$buf = @fopen($path, 'rb');
	if (! $buf) {
		header('HTTP/1.1 500 Internal Server Error');
		die();
	}
	
	fseek($buf, $seek_start);
	while (! feof($buf)) {
		echo @fread($buf, 8196);
		ob_flush();
		flush();
		
		if (connection_status() != 0) {
			@fclose($buf);
			die();
		}
	}
	
	@fclose($buf);
	die();
}

$access = null;
if (array_key_exists('token', $_REQUEST)) {
	$access = Access::load($_REQUEST['token']);
}

if (is_object($access)) {
	$path = $config['storage']['projects'].$access->getPath();
	Logger::info("download ".$access->getPath()." size: ".number_format(filesize($path)));
	$access->remove();
	result_download_request($path, basename($path), false);
}
