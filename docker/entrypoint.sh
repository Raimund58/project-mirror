#!/bin/bash
set -e

if [ "$1" = 'apache2' ]; then
    : ${APACHE_CONFDIR:=/etc/apache2}
    : ${APACHE_ENVVARS:=$APACHE_CONFDIR/envvars}

    if test -f $APACHE_ENVVARS; then
        . $APACHE_ENVVARS
    fi

    mkdir -p $APACHE_RUN_DIR $APACHE_LOCK_DIR
    chown $APACHE_RUN_USER $APACHE_LOCK_DIR

    if [ "$2" = '-DFOREGROUND' ]; then
        /etc/init.d/cron start
        /etc/init.d/php7.3-fpm start
    fi
fi

exec "$@"
